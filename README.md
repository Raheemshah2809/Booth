# The Booth
![GitHub commit checks state](https://img.shields.io/github/checks-status/Raheemshah2809/booth/711ecc21465c09d0aa07f824ae1941c3c9d5dd12)
![GitHub commit activity](https://img.shields.io/github/commit-activity/m/Raheemshah2809/booth)
![GitHub last commit](https://img.shields.io/github/last-commit/Raheemshah2809/booth)
![GitHub Release Date](https://img.shields.io/github/release-date/Raheemshah2809/Booth)

 Client Website 


Version 1.4 Live. 
What's new! 

Finally, the website is now live and in a finished state. Here are some changes from the v1.3 (not PR) 

Brand new Contact page, Revamped with cards.
Removed Footer due to a bug that needs to be fixed with it. 
Navbar not be added to the full website due to a decision and testing I thought it wasn't correct. 
Logo and Nav menu tweaked and made a lot better.
Removed Google form and connected clients page instead.
Brand new Testimonials bar which slides. 
Changed intro jumbotron to the clients liking.
Added a new About us Page which has the longer version of the intro.
Updated Opening Times page with official times and additional text. 
The portfolio page got a major revamp added a new card style and the hover effect removed the old list style. 
Updated Opening times and did some minor CSS work.
Updated footer with a correct version number.
Added a new URL to which the site can be accessed to.
Theboothuk.com & nabzcutz.com

From this version forward, updates will now be weekly rather than daily. due to ongoing projects elsewhere, If something is urgent this will be put first. Other than that minor tweaks or changes will be weekly every Sunday-Monday.

Version - 1.5 (Due March-April 2021) Delayed! Now <del>Expected July-September 2021</del> Now Expected July-December 2021
pending: 
Footer to be brought back on the contact page. (Working For September 2021)
A new hamburger menu along with a new CTA menu. (Scrapped, Not Needed)
Adding Google analytic data to monitor and view traffic. (Done)
The site should be fully WC3 validated by then. (Working For September 2021 - December 2021)
Added Cookies are being used and Google Analytics are also being used. (Working for December 2021)

Version 1.9 - 2.0 (Pre-Alpha) 
Move the site AWS with a working PHP server which brings a working contact form and a lot more possibilities, this is currently in the works and not be released anytime soon.  <del> Expected Summer 2022-2023, Delayed By 12-24 Months Due to Ongoing Projects, Priority Low</del>
<del>Now Expected Summer 2023, Version 2.0 of the site may be cancelled. Update in 2023. </del> <br>
Version 1.9 - 2.0 Now Cancelled! (Updated February 2022)

This Changelog Was Last Updated In February 2022 
Version 1.5-1.6.X Will Be the Last Major Updates To The Site. (EOL - December 2022) 
Only SSL Certificate Will be renewed every 90 days. 
